/** 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef _C_DISTRIBUTIONS_H
#define _C_DISTRIBUTIONS_H

#include <rng.h>

#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

struct Normal {double loc; double scale;};
typedef struct Normal Normal;
Normal make_Normal(double loc, double scale);
double sample_Normal(Normal * dist, XOR64State * state);
double logprob_Normal(Normal * dist, double value);

struct Categorical { double * prob; double * cum_prob; unsigned dim; };
typedef struct Categorical Categorical;
Categorical make_Categorical(double * prob, double * cum_prob, unsigned dim);
unsigned sample_Categorical(Categorical * dist, XOR64State * state);
double logprob_Categorical(Categorical * dist, unsigned value);

struct Gamma {double k; double theta; Normal norm;};
typedef struct Gamma Gamma;
Gamma make_Gamma(double k, double theta);
double sample_Gamma(Gamma * dist, XOR64State * state);
double logprob_Gamma(Gamma * dist, double value);

#endif  // _C_DISTRIBUTIONS_H
