/** 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef _C_RV_H
#define _C_RV_H

#include <rng.h>
#include <distributions.h>

/**
 * Defined for convenience. In reality, users will likely have their
 * own semantic address types that they want to use, which will be
 * cast to void * in an address's address type.
 */
typedef enum {STATIC, CHOICE, CONTINUE, PARAMETER} address_type;

typedef enum {SAMPLED, OBSERVED, REPLAYED, PROPOSED} Interpretation;

typedef struct address { 
    unsigned address_number; void * address_type;
} address;

address make_static_address(unsigned num);
address make_choice_address(unsigned num);
address make_continue_address(unsigned num);
address make_parameter_address(unsigned num);
int get_address_type(address * a);

typedef enum {UNSIGNED_RV, DOUBLE_RV} rv_type;

typedef struct _unsigned_rv {
    address addr;
    unsigned value;
    double logprob;
    Interpretation interp;
    void * dist;
    double (*score_fn)(void *, unsigned);
} unsigned_rv;

typedef struct _double_rv {
    address addr;
    double value;
    double logprob;
    Interpretation interp;
    void * dist;
    double (*score_fn)(void *, double);
} double_rv;

double_rv sample_NormalRV_ref(address addr, Normal * dist_ref, XOR64State * state);
double_rv observe_NormalRV_ref(address addr, Normal * dist_ref, double value);
double_rv propose_normalRV_ref(address addr, Normal * dist_ref, double value);
double_rv sample_NormalRV(address addr, Normal dist, XOR64State * state);
double_rv observe_NormalRV(address addr, Normal dist, double value);
double_rv propose_NormalRV(address addr, Normal dist, double value);

double_rv sample_GammaRV_ref(address addr, Gamma * dist_ref, XOR64State * state);
double_rv observe_GammaRV_ref(address addr, Gamma * dist_ref, double value);
double_rv propose_GammaRV_ref(address addr, Gamma * dist_ref, double value);
double_rv sample_GammaRV(address addr, Gamma dist, XOR64State * state);
double_rv observe_GammaRV(address addr, Gamma dist, double value);
double_rv propose_GammaRV(address addr, Gamma dist, double value);

unsigned_rv sample_CategoricalRV_ref(address addr, Categorical * dist_ref, XOR64State * state);
unsigned_rv observe_CategoricalRV_ref(address addr, Categorical * dist_ref, unsigned value);
unsigned_rv propose_CategoricalRV_ref(address addr, Categorical * dist_ref, unsigned value);
unsigned_rv sample_CategoricalRV(address addr, Categorical dist, XOR64State * state);
unsigned_rv observe_CategoricalRV(address addr, Categorical dist, unsigned value);
unsigned_rv propose_CategoricalRV(address addr, Categorical dist, unsigned value);

double_rv propose_ImproperDoubleUniformRV(address addr, double value);
unsigned_rv propose_ImproperUnsignedUniformRV(address addr, unsigned value);

#endif  // _C_RV_H
