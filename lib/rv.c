/** 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <rng.h>
#include <distributions.h>
#include <rv.h>

address_type __STATIC = STATIC;
address_type __CHOICE = CHOICE;
address_type __CONTINUE = CONTINUE;
address_type __PARAMETER = PARAMETER;

Interpretation __SAMPLED = SAMPLED;
Interpretation __OBSERVED = OBSERVED;
Interpretation __REPLAYED = REPLAYED;

address make_static_address(unsigned num) {
    address a = {
      num, (void *) &__STATIC 
    };
    return a;
}

address make_choice_address(unsigned num) {
    address a = {
      num, (void *) &__CHOICE 
    };
    return a;
}

address make_continue_address(unsigned num) {
    address a = {
      num, (void *) &__CONTINUE
    };
    return a;
}

address make_parameter_address(unsigned num) {
    address a = {
      num, (void *) &__PARAMETER 
    };
    return a;
}

int get_address_type(address * a) {
    return *(int *)a->address_type;
}

double _score_fn_Normal(void * dist, double value) {
    return logprob_Normal((Normal *) dist, value);
}

double_rv sample_NormalRV_ref(address addr, Normal * dist_ref, XOR64State * state) {
    double value = sample_Normal(dist_ref, state);
    double lp = logprob_Normal(dist_ref, value);
    double_rv r = {
        addr, value, lp, __SAMPLED, (void *) dist_ref, _score_fn_Normal
    };
    return r;
}

double_rv observe_NormalRV_ref(address addr, Normal * dist_ref, double value) {
    double lp = logprob_Normal(dist_ref, value);
    double_rv r = {
        addr, value, lp, __OBSERVED, (void *) dist_ref, _score_fn_Normal
    };
    return r;
}

double_rv propose_normalRV_ref(address addr, Normal * dist_ref, double value) {
    double lp = logprob_Normal(dist_ref, value);
    double_rv r = {
        addr, value, lp, PROPOSED, (void *) dist_ref, _score_fn_Normal
    };
    return r;
}

double_rv sample_NormalRV(address addr, Normal dist, XOR64State * state) {
    return sample_NormalRV_ref(addr, &dist, state);
}

double_rv observe_NormalRV(address addr, Normal dist, double value) {
    return observe_NormalRV_ref(addr, &dist, value);
}

double_rv propose_NormalRV(address addr, Normal dist, double value) {
    return propose_normalRV_ref(addr, &dist, value);
}

double _score_fn_Gamma(void * dist, double value) {
    return logprob_Gamma((Gamma *) dist, value);
}

double_rv sample_GammaRV_ref(address addr, Gamma * dist_ref, XOR64State * state) {
    double value = sample_Gamma(dist_ref, state);
    double lp = logprob_Gamma(dist_ref, value);
    double_rv r = {
        addr, value, lp, __SAMPLED, (void *) dist_ref, _score_fn_Gamma
    };
    return r;
}

double_rv observe_GammaRV_ref(address addr, Gamma * dist_ref, double value) {
    double lp = logprob_Gamma(dist_ref, value);
    double_rv r = {
        addr, value, lp, __OBSERVED,  (void *) dist_ref, _score_fn_Gamma
    };
    return r;
}

double_rv propose_GammaRV_ref(address addr, Gamma * dist_ref, double value) {
    double lp = logprob_Gamma(dist_ref, value);
    double_rv r = {
        addr, value, lp, PROPOSED, (void *) dist_ref, _score_fn_Gamma
    };
    return r;
}

double_rv sample_GammaRV(address addr, Gamma dist, XOR64State * state) {
    return sample_GammaRV_ref(addr, &dist, state);
}

double_rv observe_GammaRV(address addr, Gamma dist, double value) {
    return observe_GammaRV_ref(addr, &dist, value);
}

double_rv propose_GammaRV(address addr, Gamma dist, double value) {
    return propose_GammaRV_ref(addr, &dist, value);
}

double _score_fn_Categorical(void * dist, unsigned value) {
    return logprob_Categorical((Categorical *) dist, value);
}

unsigned_rv sample_CategoricalRV_ref(address addr, Categorical * dist_ref, XOR64State * state) {
    unsigned value = sample_Categorical(dist_ref, state);
    double lp = logprob_Categorical(dist_ref, value);
    unsigned_rv r = {
        addr, value, lp, __SAMPLED, (void *) dist_ref, _score_fn_Categorical
    };
    return r;
}

unsigned_rv observe_CategoricalRV_ref(address addr, Categorical * dist_ref, unsigned value) {
    double lp = logprob_Categorical(dist_ref, value);
    unsigned_rv r = {
        addr, value, lp, __OBSERVED, (void *) dist_ref, _score_fn_Categorical
    };
    return r;
}

unsigned_rv propose_CategoricalRV_ref(address addr, Categorical * dist_ref, unsigned value) {
    double lp = logprob_Categorical(dist_ref, value);
    unsigned_rv r = {
        addr, value, lp, PROPOSED, (void *) dist_ref, _score_fn_Categorical
    };
    return r;
}

unsigned_rv sample_CategoricalRV(address addr, Categorical dist, XOR64State * state) {
    return sample_CategoricalRV_ref(addr, &dist, state);
}

unsigned_rv observe_CategoricalRV(address addr, Categorical dist, unsigned value) {
    return observe_CategoricalRV_ref(addr, &dist, value);
}

unsigned_rv propose_CategoricalRV(address addr, Categorical dist, unsigned value) {
    return observe_CategoricalRV_ref(addr, &dist, value);
}

double _score_fn_ImproperDouble(void * dist, double value) {
    (void) dist;
    (void) value;
    return 0.0;
}

double _score_fn_ImproperUnsigned(void * dist, unsigned value) {
    (void) dist;
    (void) value;
    return 0.0;
}

double_rv propose_ImproperDoubleUniformRV(address addr, double value) {
    double_rv r = {
        addr, value, 0.0, PROPOSED, (void *) 0x0, _score_fn_ImproperDouble
    };
    return r;
}

unsigned_rv propose_ImproperUnsignedUniformRV(address addr, unsigned value) {
    unsigned_rv r = {
        addr, value, 0.0, PROPOSED, (void *) 0x0, _score_fn_ImproperUnsigned
    };
    return r;
}
