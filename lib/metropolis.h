/** 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef _C_METROPOLIS_H
#define _C_METROPOLIS_H

#include <rng.h>
#include <distributions.h>
#include <rv.h>
#include <inference_foundation.h>

/**
 * TODO: move this signature out to a more general part of inference
 */ 
typedef score_t (*_metropolis_program)(inference_manager *, void *);

/**
 * TODO: potentially move this signature out to a more general part of inference
 * 
 * Arguments:
 * 1. An inference manager
 * 2. A score type -- e.g., for posterior sampling
 * 3. An integer denoting whether the new sample was accepted or not
 * 4. An unsigned denoting the amount of computation (e.g., iterations)
 *  performed thus far
 */ 
typedef void (*_post_step_program)(inference_manager *, score_t *, int, unsigned);

void _prior_metropolis(
    _metropolis_program f,
    inference_manager * im,
    _post_step_program psp,
    void * data,
    unsigned num_iterations,
    XOR64State * rng_state
);


#endif  // _C_METROPOLIS_H
