/** 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */


#include <rng.h>
#include <distributions.h>
#include <rv.h>
#include <inference_foundation.h>
#include <metropolis.h>

#include <math.h>

/**
 * NOTE: we probably want to change this to an iterable of inference manager
 */ 
void _prior_metropolis(
    _metropolis_program f,
    inference_manager * im,
    _post_step_program psp,
    void * data,
    unsigned num_iterations,
    XOR64State * rng_state
) {
    score_t scr = make_score();
    scr.loglikelihood = -1.0 * INFINITY;
    scr.logprob = -1.0 * INFINITY;
    score_t last_scr = scr;
    double log_acceptance_ratio;

    for (unsigned ix = 0; ix != num_iterations; ix++) {
        scr = f(im, data);
        // Generic computation is log p(x, z') + q(z' | z) - [p(x, z) + q(z | z')]
        // But, in this case, since we propose from the prior p(z) and p(z'), we can
        // simplify to log p(x | z') - log p(x | z).
        log_acceptance_ratio = scr.loglikelihood - last_scr.loglikelihood;
        if (log(randu_xorshift64s(rng_state)) < log_acceptance_ratio) {
            psp(im, &scr, 1, ix);
        } else {
            psp(im, &scr, 0, ix);
        }
        last_scr = scr;
    }
}
