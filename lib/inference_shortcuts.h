/** 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef _C_INFERENCE_SHORTCUTS_H
#define _C_INFERENCE_SHORTCUTS_H


#define DEFINE_SINGLE_SITE_RESULTS(name, type) \
    typedef struct __ ## name ## _results { \
        type name ## _current; \
        type name ## _new; \
        type * name ## _samples; \
    } __ ## name ## _results

#define INSERT_SAMPLE_INSTRUMENTATION \
    unsigned num_samples_collected; \
    unsigned iteration_num

#define INSERT_SET_CURRENT(the_rv, the_address_num, name) \
    if (the_rv->addr.address_number == the_address_num) { \
        if (the_results->iteration_num == 0) { \
            the_results->name ## _results->name ## _current = the_rv->value; \
        } \
        the_results->name ## _results->name ## _new = the_rv->value; \
    }

#define INSERT_ACCEPT_SAMPLE(the_results, name) \
    the_results->name##_results->name##_current = the_results->name##_results->name##_new;

#define INSERT_BURNIN_THIN(the_results, burn_in, thin, num_iterations) \
    ((iteration_num >= burn_in) && \
        (iteration_num % thin == 0UL) && \
        (the_results->iteration_num < num_iterations))

#define INSERT_RECORD_SAMPLE(the_results, name) \
    the_results->name##_results->name##_samples[the_results->num_samples_collected] = the_results->name##_results->name##_current

#endif  // _C_INFERENCE_SHORTCUTS_H