/** 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef _C_MODEL_SHORTCUTS_H
#define _C_MODEL_SHORTCUTS_H

// Semantics for random variables that always occur. A program
// composed of only STATIC random variables could be compiled
// to a graph.

#define STATIC_SAMPLE_DOUBLE(addr, name, mgr, scr, dist, rng, ...) \
    double_rv name = sample_ ##dist## RV( \
        make_static_address(addr), \
        make_ ## dist(__VA_ARGS__), rng  \
    ); \
    manage_inference(mgr, DOUBLE_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define STATIC_OBSERVE_DOUBLE(addr, name, mgr, scr, dist, value, ...) \
    double_rv name = observe_ ##dist## RV( \
        make_static_address(addr), \
        make_ ## dist(__VA_ARGS__), value  \
    ); \
    manage_inference(mgr, DOUBLE_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define STATIC_SAMPLE_UNSIGNED(addr, name, mgr, scr, dist, rng, ...) \
    unsigned_rv name = sample_ ##dist## RV( \
        make_static_address(addr), \
        make_ ## dist(__VA_ARGS__), rng  \
    ); \
    manage_inference(mgr, UNSIGNED_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define STATIC_OBSERVE_UNSIGNED(addr, name, mgr, scr, dist, value, ...) \
    unsigned_rv name = observe_ ##dist## RV( \
        make_static_address(addr), \
        make_ ## dist(__VA_ARGS__), value  \
    ); \
    manage_inference(mgr, UNSIGNED_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

// Semantics for random variables that generate branching structure

#define CHOICE_SAMPLE_DOUBLE(addr, name, mgr, scr, dist, rng, ...) \
    double_rv name = sample_ ##dist## RV( \
        make_choice_address(addr), \
        make_ ## dist(__VA_ARGS__), rng  \
    ); \
    manage_inference(mgr, DOUBLE_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define CHOICE_OBSERVE_DOUBLE(addr, name, mgr, scr, dist, value, ...) \
    double_rv name = observe_ ##dist## RV( \
        make_choice_address(addr), \
        make_ ## dist(__VA_ARGS__), value  \
    ); \
    manage_inference(mgr, DOUBLE_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define CHOICE_SAMPLE_UNSIGNED(addr, name, mgr, scr, dist, rng, ...) \
    unsigned_rv name = sample_ ##dist## RV( \
        make_choice_address(addr), \
        make_ ## dist(__VA_ARGS__), rng  \
    ); \
    manage_inference(mgr, UNSIGNED_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define CHOICE_OBSERVE_UNSIGNED(addr, name, mgr, scr, dist, value, ...) \
    unsigned_rv name = observe_ ##dist## RV( \
        make_choice_address(addr), \
        make_ ## dist(__VA_ARGS__), value  \
    ); \
    manage_inference(mgr, UNSIGNED_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

// Semantics for random variables that only conditionally occur

#define CONTINUE_SAMPLE_DOUBLE(addr, name, mgr, scr, dist, rng, ...) \
    double_rv name = sample_ ##dist## RV( \
        make_continue_address(addr), \
        make_ ## dist(__VA_ARGS__), rng  \
    ); \
    manage_inference(mgr, DOUBLE_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define CONTINUE_OBSERVE_DOUBLE(addr, name, mgr, scr, dist, value, ...) \
    double_rv name = observe_ ##dist## RV( \
        make_continue_address(addr), \
        make_ ## dist(__VA_ARGS__), value  \
    ); \
    manage_inference(mgr, DOUBLE_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define CONTINUE_SAMPLE_UNSIGNED(addr, name, mgr, scr, dist, rng, ...) \
    unsigned_rv name = sample_ ##dist## RV( \
        make_continue_address(addr), \
        make_ ## dist(__VA_ARGS__), rng  \
    ); \
    manage_inference(mgr, UNSIGNED_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define CONTINUE_OBSERVE_UNSIGNED(addr, name, mgr, scr, dist, value, ...) \
    unsigned_rv name = observe_ ##dist## RV( \
        make_continue_address(addr), \
        make_ ## dist(__VA_ARGS__), value  \
    ); \
    manage_inference(mgr, UNSIGNED_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

// Parameters -- random variables with improper priors
// Can be treated as *deterministic* parameters by using MLE
// algorithms instead of full sampling

#define DOUBLE_PARAMETER(addr, name, mgr, scr, value) \
    double_rv name = propose_ImproperDoubleUniformRV( \
        make_parameter_address(addr), \
        value  \
    ); \
    manage_inference(mgr, DOUBLE_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#define UNSIGNED_PARAMETER(addr, name, mgr, scr, value) \
    unsigned_rv name = propose_ImproperUnsignedUniformRV( \
        make_parameter_address(addr), \
        value  \
    ); \
    manage_inference(mgr, UNSIGNED_RV, &name); \
    scr = accumulate_score(scr, name.logprob, name.interp)

#endif  // _C_MODEL_SHORTCUTS_H
