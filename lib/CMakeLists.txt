cmake_minimum_required(VERSION 3.20.0)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REQUIRED True)

# set the project name and version
project(cpolymorphic VERSION 0.0.1)

add_library(${PROJECT_NAME}  
            rng.c
            distributions.c
            rv.c
            inference_foundation.c
            metropolis.c
)

target_include_directories(${PROJECT_NAME} PRIVATE ./)
target_include_directories(${PROJECT_NAME} SYSTEM INTERFACE ./)

target_compile_options(${PROJECT_NAME} PRIVATE -g -O0 -Wall -pedantic-errors -Werror -Wextra -Wconversion -Wsign-conversion)
