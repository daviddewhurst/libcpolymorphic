/* 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#define _USE_MATH_DEFINES

#include <rng.h>
#include <distributions.h>

#include <stdint.h>
#include <math.h>

#define __TWO_PI 2.0 * M_PI

Normal make_Normal(double loc, double scale) { 
    Normal norm = {loc, scale};
    return norm;
}

double sample_Normal(Normal * dist, XOR64State * state) {
    double u = randu_xorshift64s(state);
    // this is inefficient and we'd want to save the second value somewhere
    double v = randu_xorshift64s(state);  
    return dist->loc + dist->scale * sqrt(-2.0 * log(u)) * cos(__TWO_PI * v);
}

double logprob_Normal(Normal * dist, double value) {
    return -0.5 * pow((value - dist->loc) / dist->scale, 2.0) - (
        0.5 * log(__TWO_PI) + log(dist->scale)
    );
}

Categorical make_Categorical(double * prob, double * cum_prob, unsigned dim) {
    Categorical dist = {prob, cum_prob, dim};
    return dist;
}

unsigned sample_Categorical(Categorical * dist, XOR64State * state) {
    unsigned left = 0;
    unsigned right = dist->dim - 1;
    unsigned mid;
    double u = randu_xorshift64s(state);

    while (left < right) {
        mid = (left + right) / 2;
        if (dist->cum_prob[mid] < u) {
            left += 1;
        } else {
            right = mid;
        }
    }
    return left;
}

double logprob_Categorical(Categorical * dist, unsigned value) {
    return log(dist->prob[value]);
}

Gamma make_Gamma(double k, double theta) {
    Normal norm = {0.0, 1.0};
    Gamma dist = {k, theta, norm};
    return dist;
}

double sample_Gamma(Gamma * dist, XOR64State * state) {
    // first generate a Gamma(k, 1), then get Gamma(k, theta) by scaling
    // using Marsaglia's method -- base case works for k > 1
    // requires two rvs -- n ~ Normal(0, 1), u ~ Uniform (0, 1);
    double n, u;

    // computation of some intermediates as a function of k
    double local_k = dist->k > 1.0 ? dist->k : dist->k + 1.0;
    double d = local_k - 1.0/3.0;
    double c = 1.0 / sqrt(9.0 * d);
    double v, d_times_v, gamma_k_1;

    int stop = 0;
    while (stop < 1) {
        n = sample_Normal(&dist->norm, state);
        v = pow(1.0 + c * n, 3.0);
        d_times_v = d * v;
        u = randu_xorshift64s(state);
        if ((v > 0) & (log(u) < pow(n, 2.0)/2.0 + d - d_times_v + d * log(v))) {
            gamma_k_1 = d_times_v;
            stop++;
        }
    }

    // gamma_k = gamma_{k+1} * u^(1/k) per Marsaglia
    // note that some versions of this algorithm are wrong, the condition must be
    // <= 1.0, *not* < 1.0
    if (dist->k <= 1.0) {
        gamma_k_1 = gamma_k_1 * pow(u, 1.0 / dist->k);
    }

    // now use scaling property
    return gamma_k_1 * dist->theta;

}

double logprob_Gamma(Gamma * dist, double value) {
    return (dist->k - 1.0) * log(value) - value / dist->theta - (
        dist->k * log(dist->theta) + lgamma(dist->k)
    );
}
