/** 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <rng.h>
#include <distributions.h>
#include <rv.h>
#include <inference_foundation.h>

int null_query_filter(address query_address) { (void) query_address; return 0; }

int static_query_filter(address query_address) {
    if (get_address_type(&query_address) == STATIC) {
        return 1;
    } else {
        return 0;
    }
}

int choice_query_filter(address query_address) {
    if (get_address_type(&query_address) == CHOICE) {
        return 1;
    } else {
        return 0;
    }
}

void null_inference_processor(query_filter_t filter, rv_type rvt, void * rv, void * results) {
    (void) filter;
    (void) rvt;
    (void) rv;
    (void) results;
}

inference_manager make_inference_manager(
    query_filter_t filter,
    inference_processor process,
    void * results
) {
    inference_manager im = {
        filter, 1, 0, process, results
    };
    return im;
}

void manage_inference(
    inference_manager * m,
    rv_type rvt,
    void * value
) {
    if (m->count < m->max_iterations) {
        m->process(m->filter, rvt, value, m->results);
    }
    m->count += 1;
}

score_t make_score() {
    score_t s = {0.0, 0.0};
    return s;
}

score_t accumulate_score(score_t prev_score, double logprob, Interpretation interp) {
    double ll = 0.0;
    if (interp == OBSERVED) {
        ll += logprob;
    }
    score_t s = {prev_score.logprob + logprob, prev_score.loglikelihood + ll};
    return s;
}
