/** 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#ifndef _C_INFERENCE_FOUNDATION_H
#define _C_INFERENCE_FOUNDATION_H

#include <rng.h>
#include <distributions.h>
#include <rv.h>

/**
 * Holds two types of score:
 * 
 * 1. the log probability of the execution of the program up to that point
 * 2. the log likelihood of the execution of the program up to that point
 */ 
typedef struct _score_t{
    double logprob;
    double loglikelihood;
} score_t;

score_t make_score();

/**
 * Given a previous score, some log probability, and an interpretation,
 * return a new score.
 */ 
score_t accumulate_score(score_t prev_score, double logprob, Interpretation interp);

/**
 * Given an address, returns non-zero if inference shoudl be performed
 * for this address.
 */ 
typedef int (*query_filter_t)(address query_address);

/**
 * An inference processor takes a query address filter
 * and two void * arguments:
 * 
 * 1. an rv type (e.g., categorical_rv)
 * 2. a results type
 * 
 */ 
typedef void (*inference_processor)(query_filter_t, rv_type, void *, void *);

/**
 * Captures the state of local inference management.
 * Used to create the view of program state as program is executing. 
 * 
 * @param filter the query address filter
 * @param max_iterations the maximum number of iterations of inference to
 *  be performed
 * @param count the number of iterations of inference performed so far
 * @param process a local inference processing function
 * @param results something to contain results of inference
 */ 
typedef struct _inference_manager {
    query_filter_t filter;
    unsigned max_iterations;
    unsigned count;
    inference_processor process;
    void * results;
} inference_manager;

/**
 * Makes an inference manager struct.
 * 
 * @param filter the query address filter
 * @param inference_processor process the function locally performing inference
 * @param results something to contain results of inference
 */ 
inference_manager make_inference_manager(
    query_filter_t filter,
    inference_processor process,
    void * results
);

int null_query_filter(address query_address);
void null_inference_processor(query_filter_t filter, rv_type rvt, void * rv, void * results);

int static_query_filter(address query_address);

int choice_query_filter(address query_address);

/**
 * Manages inference for a program.
 */ 
void manage_inference(inference_manager * m, rv_type rvt, void * value);


#endif  // _C_INFERENCE_FOUNDATION_H
