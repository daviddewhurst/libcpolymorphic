/* 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <stdint.h>
#include <stdio.h>

#include <rng.h>
#include <distributions.h>
#include <rv.h>

XOR64State RNG_STATE = {20220423ULL};

int test_normal_rv() {
    printf("Before sample, RNG state is %llu\n\n", RNG_STATE.x);
    double_rv loc = sample_NormalRV(
        make_static_address(0),
        make_Normal(0.0, 1.0),
        &RNG_STATE
    );
    int address_type = get_address_type(&loc.addr);
    printf(
        "loc has address_type %d, value %f, log prob %f and interpretation %d.\n\n",
        address_type,
        loc.value,
        loc.logprob,
        loc.interp
    );
    printf("After sample, RNG state is %llu\n", RNG_STATE.x);

    Normal norm = make_Normal(0.0, 1.0);
    double_rv observed_loc = observe_NormalRV_ref(
        make_static_address(1),
        &norm,
        2.0
    );
    address_type = get_address_type(&observed_loc.addr);
    printf(
        "observed loc has address_type %d, value %f, log prob %f and interpretation %d.\n\n",
        address_type,
        observed_loc.value,
        observed_loc.logprob,
        observed_loc.interp
    );

    return 0;

}

int test_gamma_rv() {
    printf("Before sample, RNG state is %llu\n\n", RNG_STATE.x);
    double_rv g = sample_GammaRV(
        make_static_address(1),
        make_Gamma(2.0, 1.0),
        &RNG_STATE
    );
    int address_type = get_address_type(&g.addr);
    printf(
        "g has address_type %d, value %f, log prob %f and interpretation %d.\n\n",
        address_type,
        g.value,
        g.logprob,
        g.interp
    );
    printf("After sample, RNG state is %llu\n", RNG_STATE.x);

    Gamma gdist = make_Gamma(3.0, 0.5);
    double_rv observed_g = observe_GammaRV_ref(
        make_continue_address(1),
        &gdist,
        2.0
    );
    address_type = get_address_type(&observed_g.addr);
    printf(
        "observed g has address_type %d, value %f, log prob %f and interpretation %d.\n\n",
        address_type,
        observed_g.value,
        observed_g.logprob,
        observed_g.interp
    );

    return 0;
}

int test_categorical_rv() {

    double prob[] = {0.1, 0.3, 0.2, 0.4};
    double cum_prob[] = {0.1, 0.4, 0.6, 1.0};

    printf("Before sample, RNG state is %llu\n\n", RNG_STATE.x);
    unsigned_rv g = sample_CategoricalRV(
        make_choice_address(2),
        make_Categorical(&prob, &cum_prob, 4),
        &RNG_STATE
    );
    int address_type = get_address_type(&g.addr);
    printf(
        "g has address_type %d, value %d, log prob %f and interpretation %d.\n\n",
        address_type,
        g.value,
        g.logprob,
        g.interp
    );
    printf("After sample, RNG state is %llu\n", RNG_STATE.x);

    Categorical cat = make_Categorical(&prob, &cum_prob, 4);
    unsigned_rv observed_g = observe_CategoricalRV_ref(
        make_choice_address(3),
        &cat,
        3
    );
    address_type = get_address_type(&observed_g.addr);
    printf(
        "observed g has address_type %d, value %d, log prob %f and interpretation %d.\n\n",
        address_type,
        observed_g.value,
        observed_g.logprob,
        observed_g.interp
    );

    double the_score = observed_g.score_fn(observed_g.dist, 2);
    printf(
        "Scored value %d using generic score fn: %f\n\n",
        the_score
    );

    return 0;
}

int main() {
    int retval = 0;

    retval += test_normal_rv();
    retval += test_gamma_rv();
    retval += test_categorical_rv();

    return retval;
}
