/* 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <stdint.h>
#include <stdio.h>

#include <rng.h>

XOR64State RNG_STATE = {20220219ULL};

int test_xorshift64s() {
    printf("\n~~~ Testing xorshift64s ~~~\n");
    uint64_t val;
    for (int ix = 0; ix != 10; ix++) {
        val = xorshift64s(&RNG_STATE);
        printf("Iteration %d, drew random ULL value: %llu\n", ix, val);
    }

    return 0;
}

int test_randu_xorshift64s() {
    printf("\n~~~ Testing U(0,1) xorshift64s ~~~\n");
    double val;
    for (int ix = 0; ix != 10; ix++) {
        val = randu_xorshift64s(&RNG_STATE);
        printf("Iteration %d, drew random U(0,1) value: %f\n", ix, val);
    }

    return 0;
}

int main() {

    int retstatus = 0;
    retstatus += test_xorshift64s();
    retstatus += test_randu_xorshift64s();

    if (retstatus != 0) {
        printf("\nAt least one test failed, check logs.\n");
    } else {
        printf("\nAll tests passed.\n");
    }
    return retstatus;
}
