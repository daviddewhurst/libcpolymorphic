/* 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include <rng.h>
#include <distributions.h>
#include <rv.h>
#include <inference_foundation.h>
#include <model_shortcuts.h>

#include <metropolis.h>
#include <inference_shortcuts.h>


XOR64State RNG_STATE = {20220426ULL};

score_t normal_program(inference_manager * mgr, void * data) {
    double the_data = *(double *) data;
    score_t s = make_score();
    STATIC_SAMPLE_DOUBLE(0, loc, mgr, s, Normal, &RNG_STATE, 0.0, 1.0);
    STATIC_SAMPLE_DOUBLE(1, scale, mgr, s, Gamma, &RNG_STATE, 1.0, 1.0);
    STATIC_OBSERVE_DOUBLE(2, obs, mgr, s, Normal, the_data, loc.value, scale.value);
    return s;
}

static const unsigned BURN_IN = 5000;
static const unsigned THIN = 100;
static const unsigned NSAMPLES = 100;
static const unsigned NUM_ITERATIONS = 15000;

DEFINE_SINGLE_SITE_RESULTS(loc, double);
DEFINE_SINGLE_SITE_RESULTS(scale, double);

typedef struct _normal_results {
    __loc_results * loc_results;
    __scale_results * scale_results;
    INSERT_SAMPLE_INSTRUMENTATION;
} normal_results;

void normal_inference_processor(
    query_filter_t f,
    rv_type rvt,
    void * rv,
    void * results
) {
    (void) f;
    (void) rvt;

    double_rv * the_rv = (double_rv *) rv;
    normal_results * the_results = (normal_results *) results;
    // process loc and scale
    INSERT_SET_CURRENT(the_rv, 0, loc)
    INSERT_SET_CURRENT(the_rv, 1, scale)
}

void normal_post_step(
    inference_manager * im,
    score_t * scr,
    int accept,
    unsigned iteration_num
) { 
    normal_results * the_results = (normal_results *) im->results;

    // did we accept? If so, update proposed value to current
    if (accept != 0) {
        INSERT_ACCEPT_SAMPLE(the_results, loc);
        INSERT_ACCEPT_SAMPLE(the_results, scale);
    }

    // record the sample
    if INSERT_BURNIN_THIN(the_results, BURN_IN, THIN, NUM_ITERATIONS) {
        INSERT_RECORD_SAMPLE(the_results, loc);
        INSERT_RECORD_SAMPLE(the_results, scale);
        the_results->num_samples_collected += 1;
    }
    the_results->iteration_num = iteration_num;
}

int test_metropolis_normal_program() {

    double loc_samples[NSAMPLES];
    double scale_samples[NSAMPLES];

    __loc_results lr = {0.0, 0.0, &loc_samples};
    __scale_results sr = {0.0, 0.0, &scale_samples};

    double data = 3.0;

    normal_results my_results = {
        &lr, &sr, 0, 0
    };
    inference_manager normal_im = make_inference_manager(
        null_query_filter,
        normal_inference_processor,
        (void *) &my_results
    );
    normal_im.max_iterations = UINT32_MAX;

    // time it
    clock_t start = clock(), diff;
    _prior_metropolis(
        normal_program,
        &normal_im,
        normal_post_step,
        (void *) &data,
        NUM_ITERATIONS,
        &RNG_STATE
    );
    diff = clock() - start;
    double sec = (double) diff / CLOCKS_PER_SEC;
    printf("Took %lu CPU cycles (%f seconds) to run MH inference\n", diff, sec);

    printf(
        "Executed %d iterations of MH, collected %d samples (i.e., %f iterations/sec, %f samples/sec)\n",
        my_results.iteration_num, my_results.num_samples_collected,
        (double) my_results.iteration_num / sec,
        (double) my_results.num_samples_collected / sec
    );

    for (unsigned ix = 0; ix != NSAMPLES; ix = ix + 20) {
        printf("Loc %d = %f\n", ix, my_results.loc_results->loc_samples[ix]);
        printf("Scale %d = %f\n", ix, my_results.scale_results->scale_samples[ix]);
    }

    double mean_loc = 0.0;
    double mean_scale = 0.0;

    for (unsigned ix = 0; ix != NSAMPLES; ix++) {
        mean_loc += my_results.loc_results->loc_samples[ix];
        mean_scale += my_results.scale_results->scale_samples[ix];
    }
    mean_loc /= (double) NSAMPLES;
    mean_scale /= (double) NSAMPLES;
    printf("Mean loc = %f, mean scale = %f\n", mean_loc, mean_scale);

    return 0;
}

int main() {

    int retval = 0;
    retval += test_metropolis_normal_program();

    return 0;
}