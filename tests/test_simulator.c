/* 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <stdint.h>
#include <stdio.h>

#include <rng.h>
#include <distributions.h>

XOR64State RNG_STATE = {20220227ULL};

// The objective of this file is to help us think about how to 
// automatically convert posterior records from C++ into C.
// These programs will vary in efficiency. 

// Approach one: put everything into the program

/**
 * An example posterior simulator with no inputs
 */ 
double normal_program_1() {
    Normal loc = {0.5, 0.25};  // NOTE: could move out since not data dependent
    Gamma scale = {1.5, 1.0};  // NOTE: could move out since not data dependent
    double loc_value = sample_Normal(&loc, &RNG_STATE);
    double scale_value = sample_Gamma(&scale, &RNG_STATE);
    Normal data = {loc_value, scale_value};  // Depends on upstream randomness
    return sample_Normal(&data, &RNG_STATE);
}

// Approach two: moving parentless rvs to static lifetime
Normal __NP_loc = {0.5, 0.25};
Gamma __NP_scale = {1.5, 1.0};
double normal_program_2() {
    double loc_value = sample_Normal(&__NP_loc, &RNG_STATE);
    double scale_value = sample_Gamma(&__NP_scale, &RNG_STATE);
    Normal data = {loc_value, scale_value};  // Depends on upstream randomness
    return sample_Normal(&data, &RNG_STATE);
}

int test_np(unsigned num_iterations) {
    double np1_vals[num_iterations];
    double np2_vals[num_iterations];

    for (unsigned ix = 0; ix != num_iterations; ix++) {
        np1_vals[ix] = normal_program_1();
        np2_vals[ix] = normal_program_2();
    }
    

    return 0;
}


int main() {
    unsigned num_iterations = 100;
    int retval = 0;

    retval += test_np(num_iterations);

    return retval;
}