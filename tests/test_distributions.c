/* 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <stdint.h>
#include <stdio.h>

#include <rng.h>
#include <distributions.h>

XOR64State RNG_STATE = {20220227ULL};

int normal_integration_test(double loc, double scale) {
    printf("\n~~~ Testing Normal distribution using loc = %f, scale = %f ~~~\n", loc, scale);

    Normal norm = {loc, scale};
    int num_interations = 1000;
    double rv, mean_val, lp;
    for (int ix = 0; ix !=  num_interations; ix++) {
        rv = sample_Normal(&norm, &RNG_STATE);
        lp = logprob_Normal(&norm, rv);
        mean_val += rv;
        if (ix < 5) {
            printf("Sampled %f, logprob = %f\n", rv, lp);
        }
    }
    mean_val /= num_interations;
    printf("Using Normal(%f, %f), approximate mean = %f\n", norm.loc, norm.scale, mean_val);

    return 0;
}


int bernoulli_integration_test() {
    printf("\n~~~ Testing Bernoulli (2d Categorical) distribution ~~~\n");
    double coin_flip_prob[] = {0.5, 0.5};
    double cfp_cumsum[] = {0.5, 1.0};

    Categorical bern = {coin_flip_prob, cfp_cumsum, 2};
    int num_interations = 1000;
    unsigned p_0, p_1;
    double p_0f, p_1f;
    unsigned rv;
    for (int ix = 0; ix != num_interations; ix++) {
        rv = sample_Categorical(&bern, &RNG_STATE);
        if (rv == 0) {
            p_0 += 1;
        } else {
            p_1 += 1;
        }
    }
    p_0f = (double) p_0 / num_interations;
    p_1f = (double) p_1 / num_interations;
    printf("Using Bernoulli(0.5) and num_iterations = %d, estimated p_0 = %f and p_1 = %f\n",
        num_interations, p_0f, p_1f);
    double lp0 = logprob_Categorical(&bern, 0);
    double lp1 = logprob_Categorical(&bern, 1);
    printf("Using Bernoulli(0.5), logprob(0) = %f, logprob(1) = %f\n", lp0, lp1);

    return 0;
}


int categorical_integration_test() {
    printf("\n~~~ Testing Categorical distribution ~~~\n");

    double prob[] = {0.1, 0.3, 0.2, 0.4};
    double cum_prob[] = {0.1, 0.4, 0.6, 1.0};

    Categorical cat = {prob, cum_prob, 4};
    int num_interations = 1000;
    unsigned empirical[] = {0, 0, 0, 0};
    unsigned rv;

    for (unsigned ix = 0; ix != num_interations; ix++) {
        rv = sample_Categorical(&cat, &RNG_STATE);
        empirical[rv] += 1;
    }
    for (unsigned dim = 0; dim != 4; dim++) {
        printf("emperical prob[%d] = %f\n", dim, (double) empirical[dim] / num_interations);
        printf("actual prob[%d] = %f\n", dim, prob[dim]);
    }

    return 0;
}

int gamma_integration_test(double k, double theta) {
    printf("\n~~~ Testing Gamma distribution using k = %f, theta = %f ~~~\n", k, theta);
    Gamma dist = make_Gamma(k, theta);

    int num_interations = 10000;
    double rv, mean_val, lp;
    for (int ix = 0; ix !=  num_interations; ix++) {
        rv = sample_Gamma(&dist, &RNG_STATE);
        lp = logprob_Gamma(&dist, rv);
        mean_val += rv;
        if (ix < 5) {
            printf("Sampled %f, logprob = %f\n", rv, lp);
        }
    }
    mean_val /= num_interations;
    printf("Using Gamma(%f, %f), approximate mean = %f\n", dist.k, dist.theta, mean_val);

    return 0;
}


int main() {

    int retstatus = 0;

    retstatus += normal_integration_test(0.0, 1.0);
    retstatus += normal_integration_test(0.0, 5.0);
    retstatus += normal_integration_test(-2.0, 0.5);

    retstatus += bernoulli_integration_test();
    retstatus += categorical_integration_test();

    retstatus += gamma_integration_test(1.0, 1.0);
    retstatus += gamma_integration_test(0.5, 0.5);
    retstatus += gamma_integration_test(2.0, 5.0);

    if (retstatus != 0) {
        printf("\nAt least one test failed, check logs.\n");
    } else {
        printf("\nAll tests passed.\n");
    }
    return retstatus;
}
