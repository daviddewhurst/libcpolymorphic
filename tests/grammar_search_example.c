/* 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <stdint.h>
#include <stdio.h>

#include <rng.h>
#include <distributions.h>
#include <rv.h>
#include <inference_foundation.h>
#include <model_shortcuts.h>
#include <metropolis.h>

#ifndef DEFINE_ARRAY_COPY

# define DEFINE_ARRAY_COPY(type) \
    type * copy_ ## type ## _array(type const * array, size_t n) { \
        type * r = malloc(n * sizeof(type)); \
        memcpy(r, array, n * sizeof(type)); \
        return r; \
    }

#endif  // DEFINE_ARRAY_COPY

DEFINE_ARRAY_COPY(double)
DEFINE_ARRAY_COPY(unsigned)

void normalize(double * data, unsigned n) {
    
}




typedef enum {_NOOP, _NORMALIZE} op_t;




int main() {



    return 0;
}