/* 
 * This file is part of libcpolymorphic.
 * Copyright David Rushing Dewhurst and Joseph Campolongo, 2022 - present.
 * All rights reserved.
 */

#include <stdint.h>
#include <stdio.h>

#include <rng.h>
#include <distributions.h>
#include <rv.h>
#include <inference_foundation.h>
#include <model_shortcuts.h>

XOR64State RNG_STATE = {20220425ULL};

int normal_filter(address a) {
    if (a.address_number < 2) {
        return 1;
    } else {
        return 0;
    }
}

typedef struct my_results {
    double loc_sum; double scale_sum; unsigned num;
} my_results;

void my_ip(query_filter_t f, rv_type rvt, void * rv, void * results) {
    (void) rvt;
    double_rv * the_rv = (double_rv *) rv;
    my_results * r = (my_results *) results;
    if (f(the_rv->addr) != 0) {
        if (the_rv->addr.address_number == 0U) {
            r->loc_sum += the_rv->value;
        } else if (the_rv->addr.address_number == 1U) {
            r->scale_sum += the_rv->value;
        }
    }
    r->num += 1;
}

score_t normal_program(inference_manager * mgr, double data) {
    score_t s = make_score();
    STATIC_SAMPLE_DOUBLE(0, loc, mgr, s, Normal, &RNG_STATE, 0.0, 1.0);
    STATIC_SAMPLE_DOUBLE(1, scale, mgr, s, Gamma, &RNG_STATE, 1.0, 1.0);
    STATIC_OBSERVE_DOUBLE(2, obs, mgr, s, Normal, data, loc.value, scale.value);
    return s;
}

int main() {
    int retval = 0;
    my_results r = {0.0, 0.0, 0};

    inference_manager my_im = make_inference_manager(
        normal_filter,
        my_ip,
        (void *) &r
    );
    my_im.max_iterations = 5;

    score_t the_score = normal_program(&my_im, 2.0);
    printf(
        "Executed normal program.\nScore: logprob = %f, loglikelihood = %f\n",
        the_score.logprob, the_score.loglikelihood
    );
    printf(
        "Results: loc_sum = %f, scale_sum = %f, num = %d\n",
        r.loc_sum, r.scale_sum, r.num
    );

    return retval;
}

